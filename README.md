# M17UF3R1-MontoliuSandra
-- Controls
  Utilitza [W, A, S, D] o les fletxes per a desplaçarte per el mapa.
  Mou el ratolí per a moure la camera al voltant del personatge.
  Prem [C] per ajupir-te o tornar a posar-te dret.
  Prem [Shift] per a corre, funciona estant dret y ajupit.
  Prem [Space] per a saltar.
  Prem [E] per agafar objectes.
  Prem [Z,X] per moure't per l'inventari.

-- Entregues
-- R1.1.1 - Player Test: Moviment i animacions
    1.1. Utilitza models ja creats.
    1.2. Materials i textures importades correctament.
    1.3. Rigging correcte.
    2.1. Animació de Idle implementada.
    2.2. Animació de caminar dret (camina recte) implementada.
    2.3. Animació de córre dret (corre recte) implementada.
    2.4. [Extra, no implementat]
    2.5. Animació d'ajupirse, caminar ajupit i posar-se dret implementada. (+ animació de corre ajupit implementada)
    2.6. Animació de saltar implementada i quadrada amb el moviment de salt.
    2.7. [No implementat]
    2.8. [No implementat] 
    2.9. [Extra, no implementat]
    2.10. [Extra, no implementat]
    2.11. [Extra, no implementat]
    2.12. [Extra, no implementat]
    2.13. [Extra, no implementat]
    3.1. Blend tree creat per canviar entre animacions.
    3.2. Blend tree te més animacions a part del Idle base.
    4.1. Scrip que manega les animacions i moviments del personatges ("PlayerController").
    4.2. Utilitzar les animacións implementades:
        Caminar dret i ajupit: moviment básic implementat.
        Córre dret i ajupit: canvia la velocitat del moviment básic.
        Saltar: moviment de salt implementat.
        Agafar: moviment d'agafar objectes.
    4.3.1. Utilitza el CharacterControler i la funció move() per a moure al jugador.
    4.3.2. Utilitza el Input Manager, amb el teclat per controlar el moviment i el ratoli per a la direcció.
    4.4. [No implementat]
    4.5. Utilitza el Input System per al control del personatge.
    4.6. [Extra, no implementat]
-- R1.1.2 - Camera setting: Càmera i moments cinemàtics
    1.1. Càmera en tercera persona, creada amb cinemachine.
    1.2. La càmera rota sense arribar a estar mai davant del personatge.
    1.3. [No implementat]
    1.3.1. [No implementat]
    2.1. [No implementat]
    2.1.1. [Extra, no implementat]
    2.1.2. [Extra, no implementat]
-- R1.2 - Terreny
    1.2.1. Escenari exterior:
        Escenari exterior creat amb l'eina Terrain, amb llocs d'interés iun tamany de 1,5 km.
        Escenari dotat de vegetació, montanyes i arbres.
        Escenari amb dos biomes: un camí i un poble en un prat, i un poble en una platja amb llocs "secrets" a prop del mar.
        Els camins son transitables.
        Te contruccions humanes: pobles.
        S'ha a fegit al personatge creat al repte anterior.
    1.2.2. Escenari interior:
        Agafa un dels edificis utilitzats en els pobles i decora'l amb assets texturitzats.
        Creats minim 3 objectes que poden ser agafats pel usuari.
-- R1.1.3 - Camera setting
    1.3.1. Elements jugables:
        Crear 3 objectes Prefabs.
        Aquests objectes poden ser col·leccionats per l'usuari. Guardant-lo en un inventari creat amb Scriptable Object.
        Es fa saber al jugador el objecte agafat y que actualment porta "equipat".
        Crea un 4t objecte amb una interactivitat diferent. Al agafar-lo es desbloqueja una zona: apareix una porta.
        Els objectes interactuables es resalten amb un efecte de particules.
    1.3.2. Final del nivell:
        Crea una zona de final de nivell: una porta; que només es accesible amb el 4t item.
        Si l'usuari s'apropa i te l'objecte que fa de "clau" es finalitza la partida i es mostra la pantalla de final de nivell.
        El temps es para al joc per evitar que el jugador es continui movent.
    1.3.3. UI:
        Crea una escena amb una UI d'inici de joc.
        En aquesta escena hi ha escrit les instruccions de com superar el nivell (en el cas de l'escena Interior) de la porta, hi ha també una llista dels objectes colleccionables i un botó per iniciar les diferents escenes.
        Crear una UI per al final final del joc.
        En aquesta UI es mostra el missatge si s'ha guanyat, un botó per a tornar al menú i un per a reiniciar el nivell.
        Al llarg del nivell es mostra en el HUD l'inventari i els objectes col·leccionats.

    
