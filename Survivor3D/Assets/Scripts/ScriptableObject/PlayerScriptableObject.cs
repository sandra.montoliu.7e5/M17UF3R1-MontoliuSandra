using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

[CreateAssetMenu(fileName = "PlayerData", menuName = "ScriptableObject/PlayerData")]
public class PlayerScriptableObject : ScriptableObject
{
    [Header("Strength")]
    public float JumpStrength = 2.0f;
    
    [Header("Speed")]
    public float ActualSpeed;
    public float NormalSpeed = 2.0f;
    public float SprintSpeed = 5.335f;

    [Header("Scripts")]
    public PlayerController playerController;
    public PlayerBasicMovement playerBasicMovement;
    public PlayerInteract playerInteract;
    public PlayerJump playerJump;
    public PlayerCrouch playerCrouch;

    [Header("Components")]
    public PlayerInput playerInput;
    public CharacterController characterController;
    public Animator animator;

    private void OnEnable() // Per a que no es reinici al recaregar l'escena.
    {
        hideFlags = HideFlags.DontUnloadUnusedAsset;
    }
}
