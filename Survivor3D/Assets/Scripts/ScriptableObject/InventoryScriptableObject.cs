using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "InventoryData", menuName = "ScriptableObject/InventoryData")]
public class InventoryScriptableObject : ScriptableObject
{
    public List<GameObject> inventory;
    public GameObject ActualObject;

    private void OnEnable() // Per a que no es reinici al recaregar l'escena.
    {
        hideFlags = HideFlags.DontUnloadUnusedAsset;
    }
}
