using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryController : MonoBehaviour
{
    private GameObject _actualObject;
    private List<GameObject> _inventory;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("z")) ChangeObject('z');
        if (Input.GetKeyDown("x")) ChangeObject('x');
        if (Input.GetAxis("Mouse ScrollWheel") < 0f) ChangeObject('z');
        if (Input.GetAxis("Mouse ScrollWheel") > 0f) ChangeObject('x');
        
    }

    public void ChangeObject(char letter)
    {
        _inventory = Resources.Load<InventoryScriptableObject>("InventoryData").inventory;

        switch (letter)
        {
            case 'z':
                if (_actualObject == _inventory[0])
                    _actualObject = _inventory[_inventory.Count - 1];
                else _actualObject = _inventory[_inventory.IndexOf(_actualObject) - 1];
                break;
            case 'x':
                if (_actualObject == _inventory[_inventory.Count - 1])
                    _actualObject = _inventory[0];
                else _actualObject = _inventory[_inventory.IndexOf(_actualObject) + 1];
                break;
        }

        Resources.Load<InventoryScriptableObject>("InventoryData").ActualObject = _actualObject;
    }
}
