using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    
    // Scripts
    private PlayerBasicMovement _playerBasicMovement;
    private PlayerInteract _playerInteract;
    private PlayerJump _playerJump;
    private PlayerCrouch _playerCrouch;
    
    // Components
    private PlayerInput _playerInput;
    public InputAction MoveAction;

    
    

    private void Awake()
    {
        
        // Resources Components 
        Resources.Load<PlayerScriptableObject>("PlayerData").playerInput = GetComponent<PlayerInput>();
        Resources.Load<PlayerScriptableObject>("PlayerData").characterController = GetComponent<CharacterController>();
        Resources.Load<PlayerScriptableObject>("PlayerData").animator = GetComponent<Animator>();
        // Resources Scripts
        Resources.Load<PlayerScriptableObject>("PlayerData").playerController = this;
        Resources.Load<PlayerScriptableObject>("PlayerData").playerBasicMovement = GetComponent<PlayerBasicMovement>();
        Resources.Load<PlayerScriptableObject>("PlayerData").playerInteract = GetComponent<PlayerInteract>();
        Resources.Load<PlayerScriptableObject>("PlayerData").playerJump = GetComponent<PlayerJump>();
        Resources.Load<PlayerScriptableObject>("PlayerData").playerCrouch = GetComponent<PlayerCrouch>();
        

        // Variables for the script
        _playerInput = Resources.Load<PlayerScriptableObject>("PlayerData").playerInput;
        _playerBasicMovement = Resources.Load<PlayerScriptableObject>("PlayerData").playerBasicMovement;
        _playerInteract = Resources.Load<PlayerScriptableObject>("PlayerData").playerInteract;
        _playerJump = Resources.Load<PlayerScriptableObject>("PlayerData").playerJump;
        _playerCrouch = Resources.Load<PlayerScriptableObject>("PlayerData").playerCrouch;
    }

    private void OnEnable()
    {
        MoveAction = _playerInput.actions["Move"];
        _playerInput.actions["Move"].performed += ctx => _playerBasicMovement.Move(ctx);  // Moviment Basic del Player
        _playerInput.actions["Move"].canceled += ctx => _playerBasicMovement.DoNotMove(); // Quedar-se quiet
        _playerInput.actions["Interact"].performed += ctx => _playerInteract.Interact(); // Interactua amb el mon
        _playerInput.actions["Jump"].performed += ctx => _playerJump.Jump(); // Saltar
        _playerInput.actions["Crouch"].performed += ctx => _playerCrouch.Crouch(); // Agacharse
    }

    private void OnDisable()
    {
        _playerInput.actions["Move"].performed -= ctx => _playerBasicMovement.Move(ctx);
        _playerInput.actions["Move"].canceled -= ctx => _playerBasicMovement.DoNotMove();
        _playerInput.actions["Interact"].performed -= ctx => _playerInteract.Interact();
        _playerInput.actions["Jump"].performed -= ctx => _playerJump.Jump();
        _playerInput.actions["Crouch"].performed -= ctx => _playerCrouch.Crouch();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    /*
    // Update is called once per frame
    void Update()
    {
        
    }*/

    
    
}
