using System.Collections;
using System.Collections.Generic;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonController : MonoBehaviour
{
    
    public void SceneLoader(string SceneName)
    {
        EditorSceneManager.LoadScene(SceneName);
    }

    public void ExitApplication()
    {
        Application.Quit();
    }

    public void LoadSceneExterior()
    {
        SceneLoader("R1.2.1 Exteriors");
    }

    public void LoadSceneInterior()
    {
        SceneLoader("R1.2.1 Interior");
    }

    public void LoadMenu()
    {
        SceneLoader("R1.3.3 Menu");
    }
}
