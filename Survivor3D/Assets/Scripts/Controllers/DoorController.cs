using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour
{
    private SceneManager _sceneManager;

    // Start is called before the first frame update
    void Start()
    {
        _sceneManager = GameObject.Find("Manager").GetComponent<SceneManager>();    
    }

    // Update is called once per frame
    /*void Update()
    {
        
    }*/

    

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("Door");
        EndLevel();
    }

    void EndLevel()
    {
        Time.timeScale = 0;
        _sceneManager.Ending.SetActive(true);
    }
}
