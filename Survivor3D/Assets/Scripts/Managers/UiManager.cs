using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiManager : MonoBehaviour
{
    private GameObject _textActualObject;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Resources.Load<InventoryScriptableObject>("InventoryData").ActualObject != null)
            GameObject.Find("Text_ActualObject").GetComponent<Text>().text = Resources.Load<InventoryScriptableObject>("InventoryData").ActualObject.name;
    }
}
