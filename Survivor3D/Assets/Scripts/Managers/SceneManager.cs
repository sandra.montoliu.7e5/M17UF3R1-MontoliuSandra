using System.Collections;
using System.Collections.Generic;
using UnityEditor.SceneManagement;
using UnityEngine;

public class SceneManager : MonoBehaviour
{

    private List<GameObject> _inventory;
    private bool _isLocked;
    private GameObject _door;
    private GameObject _hud;
    public GameObject Ending;

    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1;

        Resources.Load<InventoryScriptableObject>("InventoryData").inventory = new List<GameObject> ();
        Resources.Load<InventoryScriptableObject>("InventoryData").ActualObject = null;

        if (EditorSceneManager.GetActiveScene().name == "R1.2.1 Interior")
        {
            _door = GameObject.Find("Door");
            _door.SetActive(false);
            _isLocked = true;

            _hud = GameObject.Find("HUD");
            Ending = GameObject.Find("Ending");
            Ending.SetActive(false);
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        if (EditorSceneManager.GetActiveScene().name == "R1.2.1 Interior")
        {
            UnlockDoor();
        }
    }

    void UnlockDoor()
    {
        _inventory = Resources.Load<InventoryScriptableObject>("InventoryData").inventory;

        if (_isLocked)
            foreach (GameObject item in _inventory)
            {
                if (item.name == "Disk")
                {
                    _isLocked = false;
                    _door.SetActive(true);
                }
            }
    }
}
