using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerJump : MonoBehaviour
{
    /*
     *  Player jump & Gravity
     */

    private float _jumpStrength; // Vertical Speed

    private CharacterController _characterController;
    private float _gravity = 9.8f;
    private float _verticalSpeed = 0;
    private Animator _animator;

    private float timer = 0.3f; // Temps que espera abans de saltar, serveix per cuadrar am animaci�.
    private float time;

    private bool canJump;

    private bool _isTimerOn;

    // Start is called before the first frame update
    void Start()
    {
        _jumpStrength = Resources.Load<PlayerScriptableObject>("PlayerData").JumpStrength;
        _characterController = Resources.Load<PlayerScriptableObject>("PlayerData").characterController;
        _animator = Resources.Load<PlayerScriptableObject>("PlayerData").animator;
        time = 0f;
        _isTimerOn = false;
    }

    // Update is called once per frame
    void Update()
    {
         Gravity();

        if (_isTimerOn) TimerOn();

        _characterController.Move(new Vector3(0f, _verticalSpeed) * Time.deltaTime);
    }

    public void Jump()
    {
        AnimationJump();
        _isTimerOn = true;
    }

    private void TimerOn()
    {
        time += Time.deltaTime;

        if (time >= timer /*&& canJump*/)
        {
            _verticalSpeed = _jumpStrength;
            _isTimerOn = false;
            time = 0f;
        }
    }

    private void AnimationJump()
    {
        _animator.SetTrigger("Jump");
    }

    private void Gravity()
    {
        _verticalSpeed -= _gravity * Time.deltaTime;

        if (_characterController.collisionFlags == CollisionFlags.Below)
        {
            //Debug.Log("Character on ground");
            _verticalSpeed = 0;
        }
    }
}
