using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteract : MonoBehaviour
{
    /*
     *  Player interacting with the world: take objects, open doors...
     */

    [SerializeField]
    private bool _canGrab;

    [SerializeField]
    private GameObject _gameObject;

    private CharacterController _characterController;

    // Start is called before the first frame update
    void Start()
    {
        _canGrab = false;
        _characterController = Resources.Load<PlayerScriptableObject>("PlayerData").characterController;
    }

    // Update is called once per frame
    void Update()
    {

    }

    

    public void Interact()
    {
        //Debug.Log("E pressed");
        if (_gameObject != null && _canGrab)
        {
            Resources.Load<InventoryScriptableObject>("InventoryData").inventory.Add(_gameObject.GetComponent<InteractuablePropController>().Prefab);
            this.gameObject.GetComponent<InventoryController>().ChangeObject('x');
            GameObject.Destroy(_gameObject);
        }
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        //Debug.Log("Collision");
        if (hit.gameObject.layer == 7) // Layer 7 = InteractuablePromp
        {
            _canGrab = true;
            _gameObject = hit.gameObject;
        }
        else
        {
            _canGrab = false;
            _gameObject = null;
        }
    }
}
