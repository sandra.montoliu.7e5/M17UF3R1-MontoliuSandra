using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerBasicMovement : MonoBehaviour
{
    /*
     *  Player basic move [W,A,S,D]
     *  Player run 
     */

    private PlayerController _playerController;

    private PlayerInput _playerInput;
    private CharacterController _characterController;
    private Animator _animator;


    [SerializeField] private Vector2 _vectorInput = Vector2.zero;
    private Vector3 _movement = Vector3.zero;
    //private float _time;
    //private float _velocitat;

    private Quaternion _playerRotation;

    // Start is called before the first frame update
    void Start()
    {
        _playerController = Resources.Load<PlayerScriptableObject>("PlayerData").playerController;
        _playerInput = Resources.Load<PlayerScriptableObject>("PlayerData").playerInput;
        _characterController = Resources.Load<PlayerScriptableObject>("PlayerData").characterController;
        _animator = Resources.Load<PlayerScriptableObject>("PlayerData").animator;
    }

    private void FixedUpdate()
    {
        Movement();
    }


    public void Movement()
    {
        // Player Rotation
        _playerRotation = Camera.main.transform.rotation;
        _playerRotation.x = 0;
        _playerRotation.z = 0;
        transform.rotation = _playerRotation;
        
        // Player movement
        float angleTo = Mathf.Atan2(_vectorInput.x, _vectorInput.y) * Mathf.Rad2Deg;
        _movement = Quaternion.Euler(0f, angleTo, 0f) * transform.forward;
        
        Sprint();
        _movement = _movement.normalized * Resources.Load<PlayerScriptableObject>("PlayerData").ActualSpeed * Time.deltaTime;

        _characterController.Move(_movement);

    }

    public void Move(InputAction.CallbackContext ctx)
    {
        _vectorInput = _playerController.MoveAction.ReadValue<Vector2>();
        Resources.Load<PlayerScriptableObject>("PlayerData").ActualSpeed = Resources.Load<PlayerScriptableObject>("PlayerData").NormalSpeed;
        //Debug.Log(ctx.duration);
        AnimationMove();
    }

    private void AnimationMove()
    {
        // Walk Animation
        _animator.SetBool("IsMoving", true);
    }

    public void DoNotMove()
    {
        _vectorInput = _playerController.MoveAction.ReadValue<Vector2>();
        Resources.Load<PlayerScriptableObject>("PlayerData").ActualSpeed = 0;
        AnimationDoNotMove();
    }

    private void AnimationDoNotMove()
    {
        // Idle Animation
        _animator.SetBool("IsMoving", false);
    }

    private void Sprint()
    {
        if (_playerInput.actions["Run"].IsPressed() && Resources.Load<PlayerScriptableObject>("PlayerData").ActualSpeed != 0)
        {
            //Debug.Log("Sprint");
            Resources.Load<PlayerScriptableObject>("PlayerData").ActualSpeed = Resources.Load<PlayerScriptableObject>("PlayerData").SprintSpeed;
            AnimationSprint();
        }
        else if (Resources.Load<PlayerScriptableObject>("PlayerData").ActualSpeed != 0)
        {
            Resources.Load<PlayerScriptableObject>("PlayerData").ActualSpeed = Resources.Load<PlayerScriptableObject>("PlayerData").NormalSpeed;
            AnimationDoNotSprint();
        }
    }

    private void AnimationSprint()
    {
        // Run Animation
        _animator.SetBool("IsMovingFast", true);
    }

    private void AnimationDoNotSprint()
    {
        _animator.SetBool("IsMovingFast", false);
    }
}
