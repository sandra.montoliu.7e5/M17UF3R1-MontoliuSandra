using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCrouch : MonoBehaviour
{
    /*
     *  Player Crouch
     */

    private Animator _animator;
    private bool _isCrouched;
    
    // Start is called before the first frame update
    void Start()
    {
        _animator = Resources.Load<PlayerScriptableObject>("PlayerData").animator;
        _isCrouched = _animator.GetBool("IsCrouched");
    }

    public void Crouch()
    {
        _isCrouched = !_isCrouched;
        _animator.SetBool("IsCrouched", _isCrouched);
    }
}
